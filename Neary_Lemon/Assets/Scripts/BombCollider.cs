﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCollider : MonoBehaviour
{
    public PlayerMovement Truck;
    public AudioSource Explosion;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bomb"))
        {

            Truck.SetTankText();
            other.gameObject.SetActive(false);
            this.transform.parent.gameObject.SetActive(false);
            Explosion.Play();


        }
    }
}
