﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public GameObject Bomb;
    public GameEnding gameEnding;
    private int NumberOfBombs = 3;
    public Text TankText;
    public Text BombText;
    private int NumberOfTanks = 6;
    
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        SetBombText();
        SetTankText();

    }

    void Update()
    {
       
       if (Input.GetKeyDown(KeyCode.Space))
        {
            if (NumberOfBombs > 0)
            {
                print("Boom!");
                Instantiate(Bomb, gameObject.transform.position, Quaternion.identity);
                NumberOfBombs = NumberOfBombs - 1;
                SetBombText();

            }
           
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        } 

    }
    public void SetBombText()
    {
        BombText.text = "Mines - " + NumberOfBombs.ToString();
    }
    
   public void SetTankText()
    {
        NumberOfTanks = NumberOfTanks - 1;
        TankText.text = "Tanks Left - " + NumberOfTanks.ToString();
        if (NumberOfTanks == 0)
        {
            gameEnding.GameWon();
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            NumberOfBombs = NumberOfBombs + 1;
            SetBombText();
        }
    }
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
        
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
    
}
